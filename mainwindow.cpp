#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    playListModel = new QStandardItemModel(this);
    ui ->playlistView ->setModel(playListModel);
    playListModel ->setHorizontalHeaderLabels(QStringList() << tr("Playlist")
                                                            << tr("File Path"));
    ui ->playlistView ->hideColumn(1);
    ui ->playlistView ->verticalHeader() ->setVisible(false);
    ui ->playlistView ->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui ->playlistView ->setSelectionMode(QAbstractItemView::SingleSelection);
    ui ->playlistView ->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui ->playlistView ->horizontalHeader() ->setStretchLastSection(true);

    player = new QMediaPlayer(this);
    playlist = new QMediaPlaylist(player);
    player ->setPlaylist(playlist);
    player ->setVolume(ui ->horizontalSlider_2 -> value());
    playlist -> setPlaybackMode(QMediaPlaylist::Loop);  // playback mode eg. ramdom, loop, next

    connect(player, &QMediaPlayer::stateChanged, this, &MainWindow::stateChanged); // time slider
    connect(player, &QMediaPlayer::positionChanged, this, &MainWindow::positionChanged); //volume slider

    connect(ui ->playlistView, &QTableView::doubleClicked, [this](const QModelIndex &index){
        playlist ->setCurrentIndex(index.row());
    });
    connect(playlist, &QMediaPlaylist::currentIndexChanged, [this](int index){
        ui ->currentTrack ->setText("Playing now - " + playListModel ->data(playListModel->index(index, 0)).toString());
    });
}

void MainWindow::stateChanged(QMediaPlayer::State state)
{
    if (state == QMediaPlayer::PlayingState) {
        ui ->playButton ->setEnabled(false);
        ui ->pauseButton ->setEnabled(true);
        ui ->stopButtton ->setEnabled(true);
    }
    else if (state == QMediaPlayer::PausedState) {
        ui ->playButton ->setEnabled(true);
        ui ->pauseButton ->setEnabled(false);
        ui ->stopButtton ->setEnabled(true);
    }
    else if (state == QMediaPlayer::StoppedState) {
        ui ->playButton ->setEnabled(true);
        ui ->pauseButton ->setEnabled(false);
        ui ->stopButtton ->setEnabled(false);
    }
}

void MainWindow::positionChanged(qint64 position)
{
    if (ui ->horizontalSlider ->maximum() != player ->duration())
        ui ->horizontalSlider ->setMaximum(player ->duration());

    ui ->horizontalSlider ->setValue(position);

    int seconds = (position/1000) % 60;
    int minutes = (position/60000) % 60;
    int hours = (position/3600000) % 24;
    QTime time(hours, minutes, seconds);
    ui ->label_2 ->setText(time.toString());
}

MainWindow::~MainWindow()
{
    delete ui;
    delete playListModel;
    delete playlist;
    delete player;
}

void MainWindow::on_playButton_clicked()
{
    player ->play();
}

void MainWindow::on_pauseButton_clicked()
{
    player ->pause();
}

void MainWindow::on_stopButtton_clicked()
{
    player ->stop();
}

void MainWindow::on_horizontalSlider_sliderMoved(int position)
{
    player ->setPosition(position);
}

void MainWindow::on_horizontalSlider_2_sliderMoved(int position)
{
    player ->setVolume(position);
}

/*
void MainWindow::on_actionOpen_File_triggered()
{
    QString filename = QFileDialog::getOpenFileName(this, "SelectAudio File", qApp ->applicationDirPath(),
                                                    "MP3 (*.mp3);; WAV (*.wav)");
    QFileInfo fileInfo(filename);

    player ->setMedia(QUrl::fromLocalFile(filename));

    if (player ->isMetaDataAvailable()) {
        QString albumTitle = player ->metaData(QMediaMetaData::AlbumTitle).toString();
        ui ->label ->setText("Playing now - " + albumTitle);
    }
    else {
        ui ->label ->setText("Playing now - " + fileInfo.fileName());
    }

    ui ->playButton ->setEnabled(true);
    ui ->playButton ->click();
}
*/

void MainWindow::on_btn_add_clicked()
{
    QStringList files = QFileDialog::getOpenFileNames(this,
                                                      tr("Open Files"),
                                                      QString(),
                                                      tr("Audio Files (*.mp3)"));

    foreach (QString filePath, files) {
        QList < QStandardItem *> items;
        items.append(new QStandardItem(QDir(filePath).dirName()));
        items.append(new QStandardItem(filePath));
        playListModel ->appendRow(items);
        playlist ->addMedia(QUrl::fromLocalFile(filePath));
    }

    ui ->playButton ->click(); // to start playing automatically
    QString songs;
    songs = playlist->mediaCount();
    ui ->media_count_label->setText(songs);  // to set number of songs in playlist

}

void MainWindow::on_btn_previous_clicked()
{
    playlist ->previous();
}

void MainWindow::on_btn_next_clicked()
{
    playlist ->next();
}

void MainWindow::on_pushButton_clicked()
{

}

void MainWindow::on_btn_shuffle_clicked()
{
    playlist ->shuffle();
}

void MainWindow::on_btn_clear_playlist_clicked()
{
    //playListModel->clear();
    //playlist->removeMedia(0);
}

void MainWindow::on_btn_save_playlist_clicked()
{

}
