#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStandardItemModel>
#include <QDebug>
#include <QFileDialog>
#include <QMediaPlayer>
#include <QMediaPlaylist>
#include <QMediaMetaData>
#include <QTime>
#include <QDir>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void stateChanged(QMediaPlayer::State state);
    void positionChanged(qint64 position);

private slots:
    void on_playButton_clicked();

    void on_pauseButton_clicked();

    void on_stopButtton_clicked();

    void on_horizontalSlider_sliderMoved(int position);

    void on_horizontalSlider_2_sliderMoved(int position);

    //void on_actionOpen_File_triggered();

    void on_btn_add_clicked();

    void on_btn_previous_clicked();

    void on_btn_next_clicked();

    void on_pushButton_clicked();

    void on_btn_shuffle_clicked();

    void on_btn_clear_playlist_clicked();

    void on_btn_save_playlist_clicked();

private:
    Ui::MainWindow *ui;
    QMediaPlayer* player;
    QStandardItemModel* playListModel;
    QMediaPlaylist* playlist;
};

#endif // MAINWINDOW_H
